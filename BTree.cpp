#include <iostream>
#include <conio.h>      
#include "BTree.h"

using namespace std;


/**************************************************
	B_TreeNode::SortingNode
****************************************************/
B_TreeNode *B_TreeNode::SortingNode(B_TreeNode *Node,unsigned long key)   // insert a key in the Node
{
	   int i=0,k=0,j=0;                                                // local variables 

	   if(Node->getSize()==0){                                    // if the number of records is zero
		 Node->setData(key,0);                                   // set the key in position 0
		 Node->setSize(1);                                       // and set as size 1
	   }
	   else{                                                    // if the node has keys
			do{ 
			  if(Node->getData(i)>key){                              // if the key in position i is greter than the key we want to insert
				 for(j=(Node->getSize()-1);j>=i;j--){            // for loop with which we set one position right from i the keys and the kids
					 Node->setData(Node->getData(j),j+1);           
					 Node->setSon(Node->getSon(j+1),j+2);
				 }
				 Node->setSon(Node->getSon(j+1),j+2);              
				 Node->setData(key,i);                             // set the key in the appropriate position i
				 Node->setSon(NULL,i);                             // set son of position i as NULL
				 k=1;                                              // set 1 in k
			  }
			  else                                                  // if the key in position i is less or equal with the key we want to insert
				 if(Node->getSize()-1==i){                           // if we are in the last position
					 Node->setData(key,i+1);                           // set the key in the last position 
					 Node->setSon(Node->getSon(i+1),i+2);              // set as kid in the position i+2 the kid of position i+1
					 Node->setSon(NULL,i+1);                           // set as kid in the position i+1 NULL
					 k=1;                                              // set 1 in k
				 }
				 i++;                                               // increase i with 1
			}while(k!=1);
			 Node->setSize(Node->getSize()+1);                      // increse the size with 1
	   }
	   return(Node);
}   

/**************************************************
	B_TreeNode::SearchKey
****************************************************/
int B_TreeNode::SearchKey(B_TreeNode *ptr,unsigned long key,int m) // search a node ptr if it has the key which we want
{
   int i=0,k=0;                                       // local variables
     do{
		 if(ptr->getSize()==0){                        // if the node is empty
			 return (m);                              // epistrefw to m 
		 }
       if(ptr->getData(i)==key){                       // if i find the key in position i
	     return(m-1);                                   // return m
	   }
        else                                          
          if(ptr->getData(i)>key){                     // if the key in position i is greater than the key we search
 			 if(ptr->getSon(i)==NULL){                // if the kid of the node in postition i is NULL
			   cout<<"THE KEY DOES NOT EXIST!!!"<<endl;  // there is not key with this price
		       return(m-1);                             // return m
			 }
             ptr=ptr->getSon(i);                       // set as ptr the appropriate kid(kid in postition i) of ptr
		     k=1;                                      // set 1 in k
			 m++;                                      // increase the m 
		  }
          else
			if((ptr->getData(i)<key) && (ptr->getData(i+1)>key) && i!=ptr->getSize()-1)  // if we find the kid which maybe has the key and we are not in the last position
			{	
 			  if(ptr->getSon(i+1)==NULL){              // if the kid of the node which maybe has the key is NULL                  
			     cout<<"THE KEY DOES NOT EXIST!!!"<<endl; // print that the key does not exist 
		         return(m-1);                                  // return m
			  }
			  ptr=ptr->getSon(i+1);                      // set as ptr the appropriate kid(kid in postition i+1) of ptr 
			  k=1;                                       // set 1 in k
			  m++;                                       // increase the m
			}
			else
			if(ptr->getData(i)<key && i==ptr->getSize()-1){                        // if the key in position i is less than the key we search
				if(ptr->getSon(i+1)==NULL){                   // if the node which maybe has the key
				 cout<<"THE KEY DOES NOT EXIST!!!"<<endl;     // print that the kid does not exist 
				return(m-1);
				}
				ptr=ptr->getSon(i+1);                         // set as ptr the appropriate kid(kid in postition i+1) of ptr
				k=1;                                          // set 1 in k
				m++;                                          // increase m
			}
			i++;                                           // increase the position with 1(go to the next position)
	 }while(k!=1);                                         // if we have find the kid which maybe has the key we search
     ptr->SearchKey(ptr,key,m);                             // call again the function searchkey
}



/**************************************************
	B_TreeRoot::InsertNode
****************************************************/
B_TreeRoot *B_TreeRoot::InsertNode(B_TreeRoot *root,unsigned long eisodoi) // insert nodes in btree,eisodoi=number of keys
{
   unsigned long key;                                          // local variable

   for(int i=0;i<eisodoi;i++){                                  // for loop create btree
       B_TreeNode *aNode=0,*ptr=0;
	   ptr=root->getRoot();

	   key=i;
	   aNode=root->SearchNode(root,ptr,key);

	  if(aNode!=0){
		aNode=aNode->SortingNode(aNode,key);
		root=root->SplitNode(aNode,root);
	  }
   }
   return(root);
}
 
/**************************************************
	B_TreeRoot::SplitNode
****************************************************/
B_TreeRoot *B_TreeRoot::SplitNode(B_TreeNode *aNode,B_TreeRoot *root) // create two nodes from one
{
	int div=0,div1=0,k=0,l=0,i=0;                           // local variables
	float resid=0,resid1=0;
	B_TreeNode *NewNode;

    if(aNode->getSize()<n){                                 // if the size of node is less than the maximum price
		 return(root);
    }
	else{                                                    // if the size has the maximum price 
		div=aNode->getSize()/2;                           
		resid=aNode->getSize()%2;
		div=div+resid;                                        // compute the size of two nodes

		if(aNode->getFather()==NULL){                           // if the node to split has not father
			B_TreeNode *father;
			father=new B_TreeNode;

			aNode->setFather(father);                            // set the father of the node with one key
			father->SortingNode(aNode->getFather(),aNode->getData(div-1));
			  
			NewNode=new B_TreeNode;                              // create a new node
			  
			for(i=0;i<div-1;i++){                             // set keys and kids in the initial node 
			 NewNode->setData(aNode->getData(i),i);
			 NewNode->setSon(aNode->getSon(i),i);
			}

			NewNode->setSon(aNode->getSon(i),i); 
			for(i=div;i<aNode->getSize();i++){                  //  set keys and kids in the new node         
			aNode->setData(aNode->getData(i),i-div);
			aNode->setSon(aNode->getSon(i),i-div);
			l++;
			}
			aNode->setSon(aNode->getSon(i),i-div);

			for(i=l;i<aNode->getSize();i++){                     // initialize the keys and the kids of the two nodes
			aNode->setData(0,i);
			aNode->setSon(NULL,i+1);
			}
			aNode->setSize(l);
			NewNode->setSize(div-1);
			NewNode->setFather(father);
			father->setSon(NewNode,0);
			father->setSon(aNode,1);
			root->setRoot(father);
		}
		else{                                               // if the node has father
		  aNode->SortingNode(aNode->getFather(),aNode->getData(div-1)); // set as key the key of node in position div -1 
	  
		  for(i=0;i<=aNode->getFather()->getSize();i++){ 
		  
			  if(aNode->getFather()->getSon(i)==NULL)                  // we find where we will put the new kid 
			  {
				  k=i;
			  }
		  }
		  NewNode=new B_TreeNode;                                     // create a new btreenode
          for(i=0;i<div-1;i++){                                     // set keys and kids in the initial node
			  NewNode->setData(aNode->getData(i),i);
			  NewNode->setSon(aNode->getSon(i),i);
		  }
		   for(i=div;i<aNode->getSize();i++){                       
             aNode->setData(aNode->getData(i),i-div);               //  set keys and kids in the new node
			 aNode->setSon(aNode->getSon(i),i-div);
			 l++;
		  }
		  for(i=l;i<aNode->getSize();i++){                           // initialize the keys and the kids of the two nodes
			  aNode->setData(0,i);
			  aNode->setSon(NULL,i+1);
		  }
		      NewNode->setSize(div-1);
			  aNode->setSize(l);
		}
	    aNode->getFather()->setSon(NewNode,k);                      // set the node of father of position k where we create a new empty kid
	    NewNode->setFather(aNode->getFather());
	}
    root->SplitNode(aNode->getFather(),root);                // make split while we go to the root of btree
    return(root);
}

/**************************************************
	B_TreeRoot::SearchNode
****************************************************/
B_TreeNode *B_TreeRoot::SearchNode(B_TreeRoot * Root,B_TreeNode *ptr,unsigned long key) // search if a node has a key
{
   int i=0,k=0;                                                            // local variables
   do{
		if(ptr->getSize()==0){                                             // if the size is zero
		 return ptr;
		}
        if(ptr->getData(i)==key){                                            // if the key exists 
	     return(0);
	   }
       else                                                               // if the key does not exist 
          if(ptr->getData(i)>key){                                          // if the key is than the key in position i                                      
 			 if(ptr->getSon(i)==NULL){                                      // if the kid in position i exists
		       return(ptr);
			 }
             ptr=ptr->getSon(i);                                            // set as ptr the kid of ptr in position i                                      
		     k=1;                                                           // set 1 in k
		  }
          else
			if((ptr->getData(i)<key) && (ptr->getData(i+1)>key) && i!=ptr->getSize()-1) // if we find the kid which maybe has the key and we are not in the last position
			{	
 			   if(ptr->getSon(i+1)==NULL){                                    // if the kid in position i+1 exists
			      return(ptr);
			   }
			   ptr=ptr->getSon(i+1);                                       // set as ptr the kid of ptr in position i+1             
			   k=1;                                                        // set 1 in k
			}
			else
			if(ptr->getData(i)<key){                                         // if the key is less than the key in position i
				if(ptr->getSon(i+1)==NULL){                                    // if the kid in position i+1 exists
					return(ptr);
				}
				ptr=ptr->getSon(i+1);                                          // set as ptr the kid of ptr in position i+1                                   
				 k=1;                                                           // set 1 in k
			}
			i++;                                                            // increase the positin with one
   }while(k!=1);
   Root->SearchNode(Root,ptr,key);                                         // call again the function searchnode as parameter the new price of ptr  
}

/**************************************************
	main
****************************************************/
int main(){ 

	unsigned long N=0,l=0;
	int prosvaseis,k=1;
	cout<<"EISAGETE TON ARITHMO TON EISODON"<<endl;
	cin>>N;

	B_TreeNode  *aNode=new B_TreeNode;
	B_TreeRoot *Root=new B_TreeRoot;

	Root->setRoot(aNode);
	Root=Root->InsertNode(Root,N);
	for(int i=0;i<49;i++){
		l=rand();
		cout<<"THE  SEARCHING NUMBER IS:"<<l<<endl;             
		prosvaseis=aNode->SearchKey(Root->getRoot(),l,k);
		cout<<"O ARITHMOS TON PROSVASEON EINAI:"<<prosvaseis<<endl<<endl;
		getch();
	}
	return 0;
}

