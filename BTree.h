/**************************************************
	Andreas Panakos BTree Search
****************************************************/

#ifndef _BTREE_H
#define _BTREE_H

#include <iostream>
#include <conio.h> 
#define n 512 


/**************************************************
	BTreeNode Class
****************************************************/
class B_TreeNode                   // the node of b_tree
{   
   private:                        // private members of node
           
	   unsigned long data[n];      // the keys of node
	   int size;                   // the numbers of records
       B_TreeNode *son[n+1];       // the kids of node 
	   B_TreeNode *father;         // the father of node

   public:                         // public members of node

	   B_TreeNode()                // constructor which initialize the private members 
	   {
		   for(int i=0;i<=n;i++)   // the pointers of kids get the price NULL
			   son[i]=NULL;     

		   for(int i=0;i<n;i++)
			   data[i]=0;          // the keys get the price zero
		       
		       father=NULL;        // the father get the price NULL
			   size=0;             // the number of records is zero
	   } 
	   ~B_TreeNode();              // destructor 
                                   
	   void setData(unsigned long dedomena,int i){data[i]=dedomena;} // accessor which inserts a key in a specific position
	   unsigned long getData(int i){return data[i];}                 // mutator which returns the key from a specific position
	   
	   void setSize(int k){size=k;}                                  // accessor which inserts the size-number of records
       int getSize(){return size;}                                   // mutator which returns the size
       
	   void setSon(B_TreeNode *Son,int i){son[i]=Son;}               // accessor which inserts a child in a specific postition 
	   B_TreeNode * getSon(int i){return son[i];}                    // mutator which returns a pointer in the child of a specific position
	  
	   void setFather(B_TreeNode *Father){father=Father;}            // acessor which inserts a pointer of the father of node
	   B_TreeNode * getFather(){return father;}                      // mutator which returns a pointer in the father

	   B_TreeNode *SortingNode(B_TreeNode *Node,unsigned long key);
       int SearchKey(B_TreeNode *ptr,unsigned long key,int m);
};


/**************************************************
	BTreeRoot Class
****************************************************/
class B_TreeRoot                                                   // the class of btree
{
   private:                                                        // private members

	   B_TreeNode *root;                                           // the root of btree

   public:                                                         // public members

	  B_TreeRoot(){root=NULL;};                                    // constructor which set the root as NULL
	 ~B_TreeRoot();                                              // destructor of btree
	   
	  void setRoot(B_TreeNode * Root){root=Root;}                 // accessor
	  B_TreeNode *getRoot(){return root;}                         // mutator

	  B_TreeRoot *InsertNode(B_TreeRoot *root,unsigned long eisodoi);
	  B_TreeRoot *SplitNode(B_TreeNode *aNode,B_TreeRoot *root);
	  B_TreeNode *SearchNode(B_TreeRoot * Root,B_TreeNode *ptr,unsigned long key);
};


#endif